/*
    This file is part of wxvnc.

    wxvnc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wxvnc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <wx/log.h>
#include "WXVNCThread.h"


using namespace wxvnc;


WX_DEFINE_LIST(WXVNCThreads);


static WXVNCThreads threads;


wxThread::ExitCode WXVNCThread::Entry()
{
    threads.Append(this);
    int result;
    ExitCode code;

    if (m_pClient) {
        wxQueueEvent(m_pEvtHandler,
                new wxThreadEvent(wxEVT_THREAD_WXVNCTHREAD_UPDATE));
        result = sdlvnc_start_client(&m_pClient);
    } else {
        result = EXIT_FAILURE;
    }

    if (result == EXIT_SUCCESS)
        code = (wxThread::ExitCode) EXIT_SUCCESS;
    else
        code = (wxThread::ExitCode) EXIT_FAILURE;

    wxQueueEvent(m_pEvtHandler,
            new wxThreadEvent(wxEVT_THREAD_WXVNCTHREAD_COMPLETE));

    wxLogDebug("Thread exits");

    return code;
}


WXVNCThread::~WXVNCThread ()
{
    threads.DeleteObject(this);
    wxLogDebug("Thread destroy");
}
