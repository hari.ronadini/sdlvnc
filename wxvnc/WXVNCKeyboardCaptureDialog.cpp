/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * WXVNCKeyboardCaptureDialog.cpp
 *
 *  Created on: Oct 16, 2016
 *      Author: rona
 */

#include "wxvncutilities.h"
#include "WXVNCKeyboardCaptureDialog.h"

#include <wx-3.0/wx/chartype.h>
#include <wx-3.0/wx/defs.h>
#include <wx-3.0/wx/event.h>
#include <wx-3.0/wx/gdicmn.h>
#include <wx-3.0/wx/gtk/textctrl.h>
#include <wx-3.0/wx/kbdstate.h>
#include <wx-3.0/wx/string.h>
#include <wx-3.0/wx/textctrl.h>

using namespace wxvnc;

WXVNCKeyboardCaptureDialog::WXVNCKeyboardCaptureDialog(wxWindow* parent, int id,
		const wxString& title, const wxPoint& pos, const wxSize& size,
		long style) :
		KeyboardCaptureDialog(parent, id, title, pos, size, style) {
	m_pClient = NULL;
	m_Scancode = 0;
	m_Unicode = '\0';
	m_bCtrlDown = false;
	m_bAltDown = false;
	m_bShiftDown = false;

	m_pScancodeText->Bind(wxEVT_CHAR_HOOK, &WXVNCKeyboardCaptureDialog::OnKeyUp,
			this);
	m_pScancodeText->SetFocus();
}

//BEGIN_EVENT_TABLE(WXVNCKeyboardCaptureDialog, KeyboardCaptureDialog)
//EVT_KEY_UP(WXVNCKeyboardCaptureDialog::OnKeyUp)
//END_EVENT_TABLE()

WXVNCKeyboardCaptureDialog::~WXVNCKeyboardCaptureDialog() {
}

void WXVNCKeyboardCaptureDialog::DoLogInput() {
	const wchar_t* chars = &m_Unicode;
	wxString myUnicode(chars);
	wxLogDebug("Key: [%s] [%s] [%c] [%c] [%c]", myUnicode.mb_str(),
			wxkeytostring(m_Scancode).mb_str(), m_bShiftDown ? 'd' : 'u',
			m_bAltDown ? 'd' : 'u', m_bCtrlDown ? 'd' : 'u');
}

void WXVNCKeyboardCaptureDialog::OnKeyUp(wxKeyEvent& event) {
	if (event.ControlDown()) {
		m_bCtrlDown = true;
	} else {
		m_bCtrlDown = false;
	}
	if (event.AltDown()) {
		m_bAltDown = true;
	} else {
		m_bAltDown = false;
	}
	if (event.ShiftDown()) {
		m_bShiftDown = true;
	} else {
		m_bShiftDown = false;
	}
	wxChar key = event.GetUnicodeKey();
	if (key != WXK_NONE) {
		m_Scancode = 0;
		m_pScancodeText->SetValue(key);
	} else {
		m_Scancode = event.GetKeyCode();
		m_pScancodeText->SetValue(wxkeytostring(m_Scancode));
	}
	m_Unicode = key;

	DoLogInput();
//	EndModal (wxID_OK);
}

wxChar WXVNCKeyboardCaptureDialog::GetUnicode() {
	return m_Unicode;
}

int WXVNCKeyboardCaptureDialog::GetScancode() {
	return m_Scancode;
}

bool WXVNCKeyboardCaptureDialog::GetCtrlDown() {
	if (m_pCtrlButton->GetValue()) {
		m_bCtrlDown = true;
	} else {
		m_bCtrlDown = false;
	}
	return m_bCtrlDown;
}

bool WXVNCKeyboardCaptureDialog::GetAltDown() {
	if (m_pAltButton->GetValue()) {
		m_bAltDown = true;
	} else {
		m_bAltDown = false;
	}
	return m_bAltDown;
}

bool WXVNCKeyboardCaptureDialog::GetShiftDown() {
	if (m_pShiftButton->GetValue()) {
		m_bShiftDown = true;
	} else {
		m_bShiftDown = false;
	}
	return m_bShiftDown;
}
