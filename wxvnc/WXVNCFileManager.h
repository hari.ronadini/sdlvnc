/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WXVNCFILEMANAGER_H__
#define __WXVNCFILEMANAGER_H__

//void storepasswd ( char *name, char *passwd);
wxCharBuffer passwdfile(char *name);
void rmpasswdfile(wxCharBuffer name);

#endif // __WXVNCFILEMANAGER_H__
