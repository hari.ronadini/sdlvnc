/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WXVNCAPP_H__
#define __WXVNCAPP_H__

#include <wx/wxprec.h>
#include <wx/cmdline.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#ifndef APP_CATALOG
#define APP_CATALOG "wxvnc"  // replace with the appropriate catalog name
#endif

#include "WXVNCLauncherFrame.h"

namespace me {
namespace huedawn {
namespace wxvnc {

class WXVNCApp: public wxApp {

public:
	virtual bool OnInit();
	virtual int OnExit();
	virtual int OnRun();
	virtual void OnInitCmdLine(wxCmdLineParser& parser);
	virtual bool OnCmdLineParsed(wxCmdLineParser& parser);

protected:
	wxLocale m_locale;  // locale we'll be using

private:
	WXVNCLauncherFrame* m_pLauncherFrame;
	wxString m_Host;

};

static const wxCmdLineEntryDesc g_cmdLineDesc[] = {

{ wxCMD_LINE_SWITCH, _("h"), _("help"), _(
		"displays help on the command line parameters"), wxCMD_LINE_VAL_NONE,
		wxCMD_LINE_OPTION_HELP },

{ wxCMD_LINE_PARAM, _("a"), _("address"), _("{host}[:port]"),
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL
				| wxCMD_LINE_NEEDS_SEPARATOR },

{ wxCMD_LINE_NONE }

};

DECLARE_APP (WXVNCApp);

}
}
}

namespace wxvnc = me::huedawn::wxvnc;

#endif // __WXVNCAPP_H__
