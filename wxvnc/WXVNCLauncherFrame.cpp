/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <libgen.h>
#include <stdlib.h>
#include <wx/log.h>
#include <wx/dcbuffer.h>
#include <wx/clipbrd.h>
#include <wx/rawbmp.h>
#include "wxvncutilities.h"
#include "WXVNCPasswordDialog.h"
#include "WXVNCLauncherFrame.h"

#define WXVNC_MENU_TERMINAL		100
#define WXVNC_MENU_SENDKEY		101
#define WXVNC_MENU_DISCONNECT	102
#define	WXVNC_MENU_EDIT			200
#define	WXVNC_MENU_PASTE		201

using namespace wxvnc;

static WXVNCLauncherFrame *s_pLauncherFrame;

wxDEFINE_EVENT(wxEVT_THREAD_WXVNCTHREAD_COMPLETE, wxThreadEvent);
wxDEFINE_EVENT(wxEVT_THREAD_WXVNCTHREAD_UPDATE, wxThreadEvent);

WXVNCLauncherFrame::WXVNCLauncherFrame(wxWindow *parent, wxWindowID id,
		const wxString &title, const wxPoint &pos, const wxSize &size,
		long style) :
		LauncherFrame(parent, id, title, pos, size, style) {
	m_iMouseBtnState = 0;
	m_lastAlt = 0;
	m_pClient = NULL;
	m_bClientInitMode = false;
	m_bClientMode = false;
	m_iClientResult = EXIT_SUCCESS;
	m_pWXVNCThread = NULL;
	m_pWXVNCThreadCS = new wxCriticalSection();
	m_ViewH = 0;
	m_ViewW = 0;
	m_ViewY = 0;
	m_ViewX = 0;
	m_pClientBitmap = NULL;
	m_bInView = false;

	// TODO Move values to definitions
	m_pTerminalMenu = m_pLauncherFrame_menubar->GetMenu(0);
	m_pSendkeyMenu = m_pTerminalMenu->FindItem(WXVNC_MENU_SENDKEY);
	m_pDisconnectMenu = m_pTerminalMenu->FindItem(WXVNC_MENU_DISCONNECT);
	m_pEditMenu = m_pLauncherFrame_menubar->GetMenu(1);
	m_pPasteMenu = m_pEditMenu->FindItem(WXVNC_MENU_PASTE);

	m_pLauncherFrame_menubar->Disable();

	DoCreateDC();

	Center();
}

BEGIN_EVENT_TABLE(WXVNCLauncherFrame, LauncherFrame)
EVT_BUTTON(wxID_ANY, WXVNCLauncherFrame::OnOK)
EVT_CLOSE (WXVNCLauncherFrame::OnClose)
EVT_MENU(wxID_ANY, WXVNCLauncherFrame::OnMenu)
EVT_SIZE(WXVNCLauncherFrame::OnResize)
EVT_PAINT(WXVNCLauncherFrame::OnPaint)
END_EVENT_TABLE()

WXVNCLauncherFrame::~WXVNCLauncherFrame() {
}

// Callbacks
char *password_callback() {
	return wxStrdup(s_pLauncherFrame->OnThreadAskPass());
}

void paint_callback(unsigned char *buffer, int x, int y, int w, int h) {
	s_pLauncherFrame->OnThreadPaint(buffer, x, y, w, h);
}

void paint_final_callback(unsigned char *buffer) {
	s_pLauncherFrame->OnThreadPaint(buffer);
}

void text_cut_callback(rfbClient *client, const char *text, int textlen) {
	s_pLauncherFrame->OnCut(wxString(text));
}

sdlvnc_size size_callback() {
	wxSize size = s_pLauncherFrame->OnViewSize();
	sdlvnc_size clientsize =
			(sdlvnc_size ) { size.GetWidth(), size.GetHeight() };
	return clientsize;
}

void resize_callback(int w, int h) {
	s_pLauncherFrame->OnViewResize(w, h);
}

void WXVNCLauncherFrame::DoConnect(wxString hostname, wxString password) {
	s_pLauncherFrame = this;

	if (!IsShown())
		Show();

	m_pStartPanel->Disable();

	m_Hostname = hostname;

	m_bClientInitMode = true;

	wxLogDebug("Initiating client: %d", GetId());

	m_pClient = sdlvnc_init_client(m_Hostname.mb_str(), 0, -1, NULL, 0,
			password_callback, paint_callback, paint_final_callback,
			size_callback, resize_callback, text_cut_callback);
	if (!m_pClient) {
		m_pStartPanel->Enable();
		m_bClientInitMode = false;
	} else {
		DoStartThread();
	}
}

void WXVNCLauncherFrame::DoStartThread() {
	m_lastAlt = 0;

	m_pStartPanel->Disable();
	m_pStartPanel->Hide();
	m_pLauncherFrame_menubar->Enable();
	m_pClientPanel->SetBackgroundStyle(wxBG_STYLE_PAINT);
	m_pClientPanel->SetSize(GetClientSize());
	OnViewResize();
	SendFramebufferUpdateRequest(m_pClient, 0, 0, m_ClientSize.GetWidth(),
			m_ClientSize.GetHeight(), FALSE);
	m_pClientPanel->Enable();
	m_pClientPanel->Show();
	m_pClientPanel->SetFocusIgnoringChildren();
	Center (wxBOTH);
	BindPanel();
	m_bClientMode = true;

	m_pWXVNCThread = new WXVNCThread(this, m_pClient);
	if (m_pWXVNCThread->Run() != wxTHREAD_NO_ERROR) {
		wxLogError("Can't create the thread!");
		m_pWXVNCThread->Delete();
		m_pWXVNCThread = NULL;
		m_pClientPanel->Disable();
		m_pClientPanel->Hide();
		m_pStartPanel->Enable();
		m_pStartPanel->Show();
		m_pStartPanel->SetFocus();
		m_bClientMode = false;
		m_bClientInitMode = false;
		return;
	}

	wxLogDebug("Thread started");
}

void WXVNCLauncherFrame::DoDisconnect() {
	sdlvnc_quit_client(m_pClient, EXIT_SUCCESS);
}

void WXVNCLauncherFrame::QuitClient() {
//	wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);
	sdlvnc_quit_client(m_pClient, EXIT_SUCCESS);
}

void WXVNCLauncherFrame::OnOK(wxCommandEvent &event) {
	wxString hostname;

	hostname = m_pHostnameText->GetValue().ToUTF8();

	DoConnect(hostname, "");
}

void WXVNCLauncherFrame::OnClose(wxCloseEvent &event) {
	wxLogDebug("Quits...");
	UnbindPanel();
	if (m_bClientInitMode) {
		wxLogDebug("Stopping initializer...");
		// TODO Kill initializer
		m_bClientInitMode = false;
	}
	if (m_bClientMode) {
		wxLogDebug("Stopping client...");
		QuitClient();
		m_pWXVNCThread->Wait();
		delete m_pWXVNCThread;
		m_pWXVNCThread = NULL;
		m_bClientMode = false;
	}
	delete m_pWXVNCThreadCS;
	if (m_pClientBitmap)
		delete m_pClientBitmap;

	wxLogDebug("Destroying frame...");
	Destroy();
}

void WXVNCLauncherFrame::OnThreadPaint(unsigned char *buffer, int x, int y,
		int w, int h) {
	wxBitmap *bmp = RGBAtoBitmap(buffer, w, h, IsBigEndian());
	wxMemoryDC bmpDC(*bmp);
	wxMemoryDC buffDC(*m_pClientBitmap);

	buffDC.Blit(x, y, w, h, &bmpDC, 0, 0);

//	wxAutoBufferedPaintDC clientDC(m_pClientPanel);
//	clientDC.StretchBlit(m_ViewX, m_ViewY, m_ViewW, m_ViewH, &buffDC, 0, 0,
//			m_ClientSize.GetWidth(), m_ClientSize.GetHeight());

	bmpDC.SelectObject(wxNullBitmap);
	delete bmp;
}

void WXVNCLauncherFrame::OnThreadPaint(unsigned char *buffer) {
	wxBitmap *bmp = RGBAtoBitmap(buffer, m_ClientSize.GetWidth(),
			m_ClientSize.GetHeight(), IsBigEndian());
	wxMemoryDC memDC(*bmp);
	wxAutoBufferedPaintDC clientDC(m_pClientPanel);
	clientDC.StretchBlit(m_ViewX, m_ViewY, m_ViewW, m_ViewH, &memDC, 0, 0,
			m_ClientSize.GetWidth(), m_ClientSize.GetHeight());

	m_pClientBuffer = buffer;

	memDC.SelectObject(wxNullBitmap);
	delete bmp;
}

wxSize WXVNCLauncherFrame::OnViewSize() {
	return m_pClientPanel->GetSize();
}

void WXVNCLauncherFrame::OnThreadComplete(wxThreadEvent &) {
	wxLogDebug("Thread exiting");

//	wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);

	UnbindPanel();

	m_pClientPanel->Disable();
	m_pClientPanel->Hide();
	m_pLauncherFrame_menubar->Disable();
	wxSize window_size = GetClientSize();
	m_pStartPanel->SetSize(0, 0, window_size.GetWidth(),
			window_size.GetHeight());
	m_pStartPanel->Show();
	m_pStartPanel->Enable();

	m_pWXVNCThread->Wait();
	delete m_pWXVNCThread;
	m_pWXVNCThread = NULL;
	m_bClientMode = false;
	m_bClientInitMode = false;

	SHOW_CURSOR;

	wxLogDebug("Thread exited!");
}

void WXVNCLauncherFrame::OnThreadUpdate(wxThreadEvent &event) {
}

wxString WXVNCLauncherFrame::OnThreadAskPass() {
	// wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);
	// wxMutexGuiEnter();

	wxString password;

	wxLogDebug("Popping dialog");

	WXVNCPasswordDialog *dlg = new WXVNCPasswordDialog(this);
	if (dlg->ShowModal() == wxID_OK) {
		password = dlg->GetPassword();
		wxLogDebug("password: %s", password == "" ? "null" : "valid");
	}

	delete dlg;

	// wxMutexGuiLeave();

	return password;
}

void WXVNCLauncherFrame::OnPanelKeyDown(wxKeyEvent &event) {
	if (!m_bClientMode)
		return;
	rfbKeySym sym;
	int code = event.GetUnicodeKey();
	if (code == WXK_NONE) {
		code = event.GetKeyCode();
		sym = wxkeytorfbkey(code);
		if (code == WXK_ALT)
			m_lastAlt = sym;
	} else {
		sym = wxunicodetorfbkey(code, event.ShiftDown());
	}
	SendKey(sym, TRUE);
}

void WXVNCLauncherFrame::OnPanelKeyUp(wxKeyEvent &event) {
	if (!m_bClientMode)
		return;
	rfbKeySym sym;
	int code = event.GetUnicodeKey();
	if (code == WXK_NONE) {
		code = event.GetKeyCode();
		sym = wxkeytorfbkey(code);
	} else {
		sym = wxunicodetorfbkey(code, event.ShiftDown());
	}
	SendKey(sym, FALSE);
}

void WXVNCLauncherFrame::OnMouseMove(wxMouseEvent &event) {
	if (!m_bClientMode)
		return;
	int x = event.GetX();
	int y = event.GetY();
	int wheelCount = 0;
	int i;
	wxPoint point(x, y);

	if (!m_bClientMode && m_bInView) {
		SHOW_CURSOR;
		m_bInView = false;
	}
	if ((x < m_ViewX || x > m_ViewW + m_ViewX || y < m_ViewY
			|| y > m_ViewH + m_ViewY) && m_bInView) {
		SHOW_CURSOR;
		m_bInView = false;
	}
	if (event.GetEventType() == wxEVT_LEAVE_WINDOW) {
		SHOW_CURSOR;
		m_bInView = false;
	}
	if ((x >= m_ViewX && x <= m_ViewW + m_ViewX && y >= m_ViewY
			&& y <= m_ViewH + m_ViewY) && !m_bInView) {
		HIDE_CURSOR;
		m_bInView = true;
	}

	if (!m_bInView)
		return;

	if (event.LeftDown() || event.LeftDClick())
		m_iMouseBtnState |= rfbButton1Mask;
	if (event.LeftUp())
		m_iMouseBtnState &= ~rfbButton1Mask;
	if (event.MiddleDown() || event.MiddleDClick())
		m_iMouseBtnState |= rfbButton2Mask;
	if (event.MiddleUp())
		m_iMouseBtnState &= ~rfbButton2Mask;
	if (event.RightDown() || event.RightDClick())
		m_iMouseBtnState |= rfbButton3Mask;
	if (event.RightUp())
		m_iMouseBtnState &= ~rfbButton3Mask;
	if (event.GetWheelRotation() > 0) {
		m_iMouseBtnState |= rfbButton4Mask;
		wheelCount = event.GetWheelRotation() / event.GetWheelDelta() - 1;
	}
	if (event.GetWheelRotation() < 0) {
		m_iMouseBtnState |= rfbButton5Mask;
		wheelCount = event.GetWheelRotation() / event.GetWheelDelta() - 1;
	}

	x = (float) (x - m_ViewX) / m_ViewScale;
	y = (float) (y - m_ViewY) / m_ViewScale;

	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;
	if (x > m_ClientSize.GetWidth())
		x = m_ClientSize.GetWidth();
	if (y > m_ClientSize.GetHeight())
		y = m_ClientSize.GetHeight();

	SendCursor(x, y, m_iMouseBtnState);
	for (i = 0; i < wheelCount; i++)
		SendCursor(x, y, m_iMouseBtnState);
	if (m_iMouseBtnState & (rfbButton4Mask | rfbButton5Mask)) {
		m_iMouseBtnState &= ~(rfbButton4Mask | rfbButton5Mask);
		SendCursor(x, y, m_iMouseBtnState);
	}
}

void WXVNCLauncherFrame::OnPanelFocusKill(wxFocusEvent &event) {
//	wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);
	if (m_bClientMode && m_lastAlt) {
		SendKey(m_lastAlt, FALSE);
		m_lastAlt = 0;
	}
	event.Skip();
}

void WXVNCLauncherFrame::OnCut(wxString string) {
	// Write some text to the clipboard
	if (wxTheClipboard->Open()) {
		// This data objects are held by the clipboard,
		// so do not delete them in the app.
		wxTheClipboard->SetData(new wxTextDataObject(string));
		wxTheClipboard->Close();
	}
}

void WXVNCLauncherFrame::DoCreateDC() {
	if (m_pClientBitmap)
		delete m_pClientBitmap;
	m_pClientBitmap = new wxBitmap(m_ClientSize);
}

void WXVNCLauncherFrame::DoPasteText() {
	// Read some text
	wxString text;
	if (wxTheClipboard->Open()) {
		if (wxTheClipboard->IsSupported(wxDF_TEXT)) {
			wxTextDataObject data;
			wxTheClipboard->GetData(data);
			//wxMessageBox(data.GetText());
			text = data.GetText();
		}
		wxTheClipboard->Close();
	}
	{
//		wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);
		sdlvnc_paste_text(m_pClient, text);
	}
}

void WXVNCLauncherFrame::OnEditMenu(wxCommandEvent &event) {
	// TODO Move values to definitions
	int id = event.GetId();

	wxLogDebug("Edit menu: %d", id);

	switch (id) {
	case WXVNC_MENU_PASTE:
		DoPasteText();
		break;
	default:
		event.Skip();
		break;
	}
	// Read some text
}

void WXVNCLauncherFrame::DoSendkey(WXVNCKeyboardCaptureDialog* dlg) {
	{
//		wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);
		if (dlg->GetCtrlDown()) {
			SendKeyEvent(m_pClient, wxkeytorfbkey(WXK_CONTROL), TRUE);
		}
		if (dlg->GetAltDown()) {
			SendKeyEvent(m_pClient, wxkeytorfbkey(WXK_ALT), TRUE);
		}
		if (dlg->GetShiftDown()) {
			SendKeyEvent(m_pClient, wxkeytorfbkey(WXK_SHIFT), TRUE);
		}
		if (dlg->GetUnicode() != WXK_NONE) {
			SendKeyEvent(m_pClient, dlg->GetUnicode(), TRUE);
		} else {
			SendKeyEvent(m_pClient, wxkeytorfbkey(dlg->GetScancode()), TRUE);
		}
		if (dlg->GetUnicode() != WXK_NONE) {
			SendKeyEvent(m_pClient, dlg->GetScancode(), FALSE);
		} else {
			SendKeyEvent(m_pClient, wxkeytorfbkey(dlg->GetScancode()), FALSE);
		}
		if (dlg->GetShiftDown()) {
			SendKeyEvent(m_pClient, wxkeytorfbkey(WXK_SHIFT), FALSE);
		}
		if (dlg->GetAltDown()) {
			SendKeyEvent(m_pClient, wxkeytorfbkey(WXK_ALT), FALSE);
		}
		if (dlg->GetCtrlDown()) {
			SendKeyEvent(m_pClient, wxkeytorfbkey(WXK_CONTROL), FALSE);
		}
	}
}

void WXVNCLauncherFrame::DoShowCaptureDialog() {
	WXVNCKeyboardCaptureDialog *dlg = new WXVNCKeyboardCaptureDialog(
			reinterpret_cast<wxWindow*>(this), wxID_ANY,
			wxT("wxvnc - Send Keyboard Combination"));
	if (dlg->ShowModal() == wxID_OK) {
		DoSendkey(dlg);
	}
}

void WXVNCLauncherFrame::OnTerminalMenu(wxCommandEvent& event) {
	// TODO Move values to definitions
	int id = event.GetId();

	wxLogDebug("Terminal menu: %d", id);

	switch (id) {
	case WXVNC_MENU_SENDKEY:
		DoShowCaptureDialog();
		break;
	default:
		event.Skip();
		break;
	}
}

void WXVNCLauncherFrame::OnMenu(wxCommandEvent& event) {
	// TODO Move values to definitions
	int id = event.GetId();

	wxLogDebug("Menu: %d", id);

	switch (id) {
	case WXVNC_MENU_TERMINAL:
		event.Skip();
		break;
	case WXVNC_MENU_SENDKEY:
		DoShowCaptureDialog();
		break;
	case WXVNC_MENU_DISCONNECT:
		DoDisconnect();
		break;
	case WXVNC_MENU_EDIT:
		event.Skip();
		break;
	case WXVNC_MENU_PASTE:
		DoPasteText();
		break;
	default:
		event.Skip();
		break;
	}
}

void WXVNCLauncherFrame::BindPanel() {
	m_pClientPanel->Bind(wxEVT_KEY_DOWN, &WXVNCLauncherFrame::OnPanelKeyDown,
			this);
	m_pClientPanel->Bind(wxEVT_KEY_UP, &WXVNCLauncherFrame::OnPanelKeyUp, this);
	m_pClientPanel->Bind(wxEVT_LEFT_DOWN, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Bind(wxEVT_LEFT_UP, &WXVNCLauncherFrame::OnMouseMove, this);
	m_pClientPanel->Bind(wxEVT_MIDDLE_DOWN, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Bind(wxEVT_MIDDLE_UP, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Bind(wxEVT_RIGHT_DOWN, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Bind(wxEVT_RIGHT_UP, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Bind(wxEVT_MOTION, &WXVNCLauncherFrame::OnMouseMove, this);
	m_pClientPanel->Bind(wxEVT_MOUSEWHEEL, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Bind(wxEVT_KILL_FOCUS,
			&WXVNCLauncherFrame::OnPanelFocusKill, this);
	Bind(wxEVT_THREAD_WXVNCTHREAD_COMPLETE,
			&WXVNCLauncherFrame::OnThreadComplete, this);
	Bind(wxEVT_THREAD_WXVNCTHREAD_UPDATE, &WXVNCLauncherFrame::OnThreadUpdate,
			this);
}

void WXVNCLauncherFrame::UnbindPanel() {
	m_pClientPanel->Unbind(wxEVT_KEY_DOWN, &WXVNCLauncherFrame::OnPanelKeyDown,
			this);
	m_pClientPanel->Unbind(wxEVT_KEY_UP, &WXVNCLauncherFrame::OnPanelKeyUp,
			this);
	m_pClientPanel->Unbind(wxEVT_MOTION, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_LEFT_DOWN, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_LEFT_UP, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_MIDDLE_DOWN, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_MIDDLE_UP, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_RIGHT_DOWN, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_RIGHT_UP, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_MOTION, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_MOUSEWHEEL, &WXVNCLauncherFrame::OnMouseMove,
			this);
	m_pClientPanel->Unbind(wxEVT_KILL_FOCUS,
			&WXVNCLauncherFrame::OnPanelFocusKill, this);
	Unbind(wxEVT_THREAD_WXVNCTHREAD_COMPLETE,
			&WXVNCLauncherFrame::OnThreadComplete, this);
	Unbind(wxEVT_THREAD_WXVNCTHREAD_UPDATE, &WXVNCLauncherFrame::OnThreadUpdate,
			this);
}

void WXVNCLauncherFrame::SendKey(rfbKeySym sym, rfbBool down) {
//	wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);
	SendKeyEvent(m_pClient, sym, down); // rfb
}

void WXVNCLauncherFrame::SendCursor(int x, int y, int state) {
	wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);
	SendPointerEvent(m_pClient, x, y, state); // rfb
}

int WXVNCLauncherFrame::IsBigEndian() {
//	wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);
	return sdlvnc_is_big_endian(m_pClient);
}

void WXVNCLauncherFrame::OnViewResize(int w, int h) {
//	wxCriticalSectionLocker enter(*m_pWXVNCThreadCS);

	float wScale, hScale;
	int panelW, panelH;

	m_ClientSize.Set(w, h);

	panelW = m_pClientPanel->GetSize().GetWidth();
	panelH = m_pClientPanel->GetSize().GetHeight();

	wScale = (float) panelW / (float) w;
	hScale = (float) panelH / (float) h;
	m_ViewScale = wScale < hScale ? wScale : hScale;

	m_ViewW = (int) ((float) w * m_ViewScale);
	m_ViewH = (int) ((float) h * m_ViewScale);

	m_ViewX = (int) ((float) (panelW - m_ViewW) / 2.0f);
	m_ViewY = (int) ((float) (panelH - m_ViewH) / 2.0f);

	wxLogDebug(
			"resize: (%d x %d) / (%d x %d) %02.2f %02.2f %02.2f %d,%d(%d x %d)",
			panelW, panelH, w, h, m_ViewScale, wScale, hScale, m_ViewX, m_ViewY,
			m_ViewW, m_ViewH);

	DoCreateDC();
}

void WXVNCLauncherFrame::OnViewResize(wxSize size) {
	OnViewResize(size.GetWidth(), size.GetHeight());
}

void WXVNCLauncherFrame::OnViewResize() {
	m_pClientPanel->SetSize(GetClientSize());
	OnViewResize (m_ClientSize);
}

void WXVNCLauncherFrame::OnResize(wxSizeEvent& event) {
	OnViewResize();
}

void WXVNCLauncherFrame::OnPaint(wxPaintEvent& event) {

	if (!m_bClientMode || !m_ClientSize.GetWidth() || !m_ClientSize.GetHeight())
		return;

	wxBitmap *bmp = RGBAtoBitmap((unsigned char *) m_pClientBuffer,
			m_ClientSize.GetWidth(), m_ClientSize.GetHeight(), IsBigEndian());
	wxMemoryDC memDC(*bmp);
	wxAutoBufferedPaintDC clientDC(m_pClientPanel);
	clientDC.StretchBlit(m_ViewX, m_ViewY, m_ViewW, m_ViewH, &memDC, 0, 0,
			m_ClientSize.GetWidth(), m_ClientSize.GetHeight());

	memDC.SelectObject(wxNullBitmap);
	delete bmp;
}
