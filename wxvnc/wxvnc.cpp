/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <wx/wx.h>
#ifdef _DEBUG
#include <mcheck.h>
#endif
#include "wxgtkutils.h"
#include "wxvnc.h"

int main(int argc, char **argv) {
	int result;

#ifdef _DEBUG
	mtrace();
#endif

#ifdef __WXGTK__
	wxvnc_init_gtk(&argc, &argv);
#endif

	result = wxEntry(argc, argv);

	return result;
}
