// -*- C++ -*-
//
// generated by wxGlade 0.7.1 on Sun Oct 16 19:23:31 2016
//
// Example for compiling a single file project under Linux using g++:
//  g++ MyApp.cpp $(wx-config --libs) $(wx-config --cxxflags) -o MyApp
//
// Example for compiling a multi file project under Linux using g++:
//  g++ main.cpp $(wx-config --libs) $(wx-config --cxxflags) -o MyApp Dialog1.cpp Frame1.cpp
//

#ifndef KEYBOARDCAPTUREDIALOG_H
#define KEYBOARDCAPTUREDIALOG_H

#include <wx/wx.h>
#include <wx/image.h>
#include <wx/intl.h>

#ifndef APP_CATALOG
#define APP_CATALOG "app"  // replace with the appropriate catalog name
#endif


// begin wxGlade: ::dependencies
#include <wx/tglbtn.h>
// end wxGlade

// begin wxGlade: ::extracode
// end wxGlade


class KeyboardCaptureDialog: public wxDialog {
public:
    // begin wxGlade: KeyboardCaptureDialog::ids
    // end wxGlade

    KeyboardCaptureDialog(wxWindow* parent, int id, const wxString& title, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);

private:
    // begin wxGlade: KeyboardCaptureDialog::methods
    void set_properties();
    void do_layout();
    // end wxGlade

protected:
    // begin wxGlade: KeyboardCaptureDialog::attributes
    wxToggleButton* m_pCtrlButton;
    wxToggleButton* m_pAltButton;
    wxToggleButton* m_pShiftButton;
    wxTextCtrl* m_pScancodeText;
    wxButton* m_pCancelButton;
    wxButton* m_pOKButton;
    // end wxGlade
}; // wxGlade: end class


#endif // KEYBOARDCAPTUREDIALOG_H
