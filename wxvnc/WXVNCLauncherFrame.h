/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WXVNCLAUNCHER_H__
#define __WXVNCLAUNCHER_H__

#include <sdlvnc_client.h>
#include "LauncherFrame.h"
#include "WXVNCThread.h"
#include "WXVNCKeyboardCaptureDialog.h"

#ifdef __cplusplus
extern "C" {
#endif

char *password_callback();
void paint_callback(unsigned char *buffer, int w, int h);
void paint_final_callback(unsigned char *buffer);
void text_cut_callback(rfbClient *client, const char *text, int textlen);
sdlvnc_size size_callback();
void resize_callback(int w, int h);

#ifdef __cplusplus
}
#endif /* __cplusplus */

namespace me {
namespace huedawn {
namespace wxvnc {

class WXVNCLauncherFrame: public LauncherFrame {
private:
	bool m_bClientInitMode;
	bool m_bClientMode;
	rfbKeySym m_lastAlt;
	int m_iMouseBtnState;
	rfbClient *m_pClient;
	int m_iClientResult;
	wxString m_Hostname;

	void QuitClient();
	void CreateMenu();
	void SendKey(rfbKeySym sym, rfbBool down);
	void SendCursor(int x, int y, int state);
	int IsBigEndian();
	void DoSendkey(WXVNCKeyboardCaptureDialog* dlg);
	void DoCreateDC();

protected:
	WXVNCThread *m_pWXVNCThread;
	wxCriticalSection *m_pWXVNCThreadCS;
	wxMenu *m_pTerminalMenu;
	wxMenuItem *m_pSendkeyMenu;
	wxMenu *m_pEditMenu;
	wxMenuItem *m_pPasteMenu;
	int m_ViewW;
	int m_ViewH;
	int m_ViewX;
	int m_ViewY;
	wxSize m_ClientSize;
	float m_ViewScale;
	wxBitmap* m_pClientBitmap;
	void* m_pClientBuffer;
	bool m_bInView;
	wxMenuItem* m_pDisconnectMenu;

	DECLARE_EVENT_TABLE();

	void BindPanel();
	void UnbindPanel();
	void OnMouseMove(wxMouseEvent &event);
	void OnPanelFocusKill(wxFocusEvent &event);
	void OnThreadComplete(wxThreadEvent &event);
	void OnThreadUpdate(wxThreadEvent &event);
	void OnPanelKeyDown(wxKeyEvent &event);
	void OnPanelKeyUp(wxKeyEvent &event);
	void OnResize(wxSizeEvent& event);
	void OnPaint(wxPaintEvent& event);
	void OnOK(wxCommandEvent &event);
	void OnMenu(wxCommandEvent& event);
	void OnTerminalMenu(wxCommandEvent& event);
	void OnEditMenu(wxCommandEvent &event);
	void DoStartThread();
	void DoDisconnect();
	void DoShowCaptureDialog();
	void DoPasteText();

public:
	WXVNCLauncherFrame(wxWindow *parent = NULL, wxWindowID id = wxID_ANY,
			const wxString &title = wxT("wxvnc"), const wxPoint &pos =
					wxDefaultPosition, const wxSize &size = wxSize(800, 600),
			long style = wxDEFAULT_FRAME_STYLE);
	~WXVNCLauncherFrame();

	void OnThreadPaint(unsigned char *buffer, int x, int y, int w, int h);
	void OnThreadPaint(unsigned char *buffer);
	void OnCut(wxString string);
	void OnViewResize(int w, int h);
	void OnViewResize(wxSize size);
	void OnViewResize();
	void OnClose(wxCloseEvent &event);
	void OnIdle(wxIdleEvent &event);
	void DoConnect(wxString hostname, wxString password);

	wxSize OnViewSize();
	wxString OnThreadAskPass();
};
}
}
}

namespace wxvnc = me::huedawn::wxvnc;

#endif // __WXVNCLAUNCHER_H__
