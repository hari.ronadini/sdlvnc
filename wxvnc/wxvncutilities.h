/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WXUTILITIES_H__
#define __WXUTILITIES_H__

#include <wx/bitmap.h>
#include <sdlvnc_client.h>

#define HIDE_CURSOR
#define SHOW_CURSOR

wxBitmap *RGBAtoBitmap(unsigned char *rgba, int w, int h, int big_endian);
rfbKeySym wxkeytorfbkey(int wx_key);
wxString wxkeytostring(int wx_key);
rfbKeySym wxunicodetorfbkey(wxChar unicode, bool shiftdown);
void checkIdle();

#endif // __WXUTILITES_H__
