/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "WXVNCPasswordDialog.h"

using namespace wxvnc;

// BEGIN_EVENT_TABLE(WXVNCPasswordDialog, wxDialog)
// END_EVENT_TABLE()

WXVNCPasswordDialog::WXVNCPasswordDialog(wxWindow* parent, wxWindowID id,
		const wxString& title, const wxPoint& pos, const wxSize& size,
		long style) :
		PasswordDialog(parent, id, title, pos, size, style) {
}

WXVNCPasswordDialog::~WXVNCPasswordDialog() {
}

int WXVNCPasswordDialog::ShowModal() {
	Centre (wxBOTH);
	int result = wxDialog::ShowModal();
	m_sPassword = m_pPasswordText->GetValue();
	m_pPasswordText->SetValue("");
	return result;
}

wxString WXVNCPasswordDialog::GetPassword() {
	return m_sPassword;
}
