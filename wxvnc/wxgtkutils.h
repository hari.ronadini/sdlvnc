/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * wxgtkutils.h
 *
 *  Created on: Oct 27, 2016
 *      Author: rona
 */

#ifndef WXGTKUTILS_H_
#define WXGTKUTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

void wxvnc_init_gtk(int *argc, char ***argv);
int wxvnc_get_id(void *hwnd);
void wxvnc_set_icon(const unsigned char buffer[], int dim, int size);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* WXGTKUTILS_H_ */
