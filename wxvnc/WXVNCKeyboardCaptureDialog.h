/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * WXVNCKeyboardCaptureDialog.h
 *
 *  Created on: Oct 16, 2016
 *      Author: rona
 */

#ifndef WXVNCKEYBOARDCAPTUREDIALOG_H_
#define WXVNCKEYBOARDCAPTUREDIALOG_H_

#include <wx/dialog.h>
#include <sdlvnc_client.h>
#include "KeyboardCaptureDialog.h"

namespace me {
namespace huedawn {
namespace wxvnc {

class WXVNCKeyboardCaptureDialog: public KeyboardCaptureDialog {
private:
	rfbClient* m_pClient;
	wxChar m_Unicode;
	int m_Scancode;
	bool m_bCtrlDown;
	bool m_bAltDown;
	bool m_bShiftDown;

protected:
	void DoLogInput();

//	DECLARE_EVENT_TABLE();

public:
	WXVNCKeyboardCaptureDialog(wxWindow* parent, int id, const wxString& title,
			const wxPoint& pos = wxDefaultPosition, const wxSize& size =
					wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
	virtual ~WXVNCKeyboardCaptureDialog();

	void OnKeyUp(wxKeyEvent& event);
	wxChar GetUnicode();
	int GetScancode();
	bool GetCtrlDown();
	bool GetAltDown();
	bool GetShiftDown();
};

} /* namespace wxvnc */
}
}

namespace wxvnc = me::huedawn::wxvnc;

#endif /* WXVNCKEYBOARDCAPTUREDIALOG_H_ */
