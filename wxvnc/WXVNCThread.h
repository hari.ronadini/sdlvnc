/*
    This file is part of wxvnc.

    wxvnc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wxvnc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __WXVNCTHREAD_H__
#define __WXVNCTHREAD_H__


#include <wx/thread.h>
#include <wx/event.h>
#include <wx/listimpl.cpp>
#include "wxvncutilities.h"


wxDECLARE_EVENT(wxEVT_THREAD_WXVNCTHREAD_COMPLETE, wxThreadEvent);
wxDECLARE_EVENT(wxEVT_THREAD_WXVNCTHREAD_UPDATE, wxThreadEvent);


namespace me { namespace huedawn { namespace wxvnc {

    class WXVNCThread : public wxThread
    {
        public:
            WXVNCThread ( wxEvtHandler *evtHandler,
                    rfbClient *client )
                    : wxThread ( wxTHREAD_JOINABLE )
                    {
                        m_pEvtHandler = evtHandler;
                        m_pClient = client;
                    }; // wxTHREAD_JOINABLE || wxTHREAD_DETACHED
            ~WXVNCThread ();

        protected:

        private:
            wxEvtHandler*   m_pEvtHandler;
            rfbClient*      m_pClient;

            wxThread::ExitCode Entry ();
    };

    WX_DECLARE_LIST(WXVNCThread, WXVNCThreads);

}}}

namespace wxvnc = me::huedawn::wxvnc;

#endif // __WXVNCTHREAD_H__
