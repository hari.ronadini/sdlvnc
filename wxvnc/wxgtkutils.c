/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * wxgtkutils.c
 *
 *  Created on: Oct 27, 2016
 *      Author: rona
 */

#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include "wxgtkutils.h"

void wxvnc_init_gtk(int *argc, char ***argv) {
//	gtk_init(argc, argv);
	XInitThreads();
}

int wxvnc_get_id(void *hwnd) {
	GtkWindow *mywindow;
	GdkWindow *gwin;

	gwin = gtk_widget_get_window(GTK_WIDGET(hwnd));

	return GDK_DRAWABLE_XID(gwin);
}

void free_pixbuf(guchar *pixels, gpointer data) {
		g_free(pixels);
		g_free(data);
}

void wxvnc_set_icon(const unsigned char buffer[], int dim, int size) {
	GdkPixbuf *icon;
	GBytes *bytes;

	bytes = g_bytes_new(buffer, size);
// 	icon = gdk_pixbuf_new_from_bytes(bytes, GDK_COLORSPACE_RGB, FALSE, 8, dim,
// 			dim, dim * 3);
	
	icon = gdk_pixbuf_new_from_data((guchar *) bytes, GDK_COLORSPACE_RGB, FALSE, 8, dim,
				dim, dim * 3, (GdkPixbufDestroyNotify) *free_pixbuf, icon);
	gtk_window_set_default_icon(icon);
}
