package me.huedawn.sdlvncclient.android;

import android.util.Log;
import org.apache.http.conn.ssl.SSLSocketFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.Socket;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by rona on 27/07/16.
 */
public class PrivateSSLSocketFactory extends SSLSocketFactory {

    private SSLContext sslContext = SSLContext.getDefault();

    public PrivateSSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
        super(truststore);
    }

    @Override
    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
    }

    @Override
    public Socket createSocket() throws IOException {
        return sslContext.getSocketFactory().createSocket();
    }

    static class PrivateX509TrustManager implements X509TrustManager {

        private boolean acceptAllClient = false;
        private boolean acceptAllServer = false;

        public static final String ANDROID_CA_STORE = "AndroidCAStore";
        public static final String INVALID_CLIENT_X509_CERTIFICATE = "Invalid client X509Certificate";
        public static final String UNIDENTIFIED_CLIENT_X509_CERTIFICATE = "Unidentified client X509Certificate";
        public static final String INVALID_SERVER_X509_CERTIFICATE = "Invalid server X509Certificate";
        public static final String UNIDENTIFIED_SERVER_X509_CERTIFICATE = "Unidentified server X509Certificate";
        public static final String UNIDENTIFIED_CLIENT_X509_CERTIFICATE_ALGORITHM = "Unidentified client X509Certificate algorithm";
        public static final String UNIDENTIFIED_SERVER_X509_CERTIFICATE_ALGORITHM = "Unidentified server X509Certificate algorithm";

        public PrivateX509TrustManager(boolean acceptAllClient, boolean acceptAllServer) {
            this.acceptAllClient = acceptAllClient;
            this.acceptAllServer = acceptAllServer;
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            X509Certificate[] issuers = getAcceptedIssuers();
            for (X509Certificate cert : chain) {
                if (cert != null) {
                    try {
                        cert.checkValidity();
                        for (X509Certificate issuer : issuers) {
                            if (cert.getSubjectDN().getName().compareTo(issuer.getSubjectDN().getName()) == 0) {
                                cert.verify(issuer.getPublicKey());
                                break;
                            }
                        }
                        Log.d(SDLVNCActivity.TAG, cert.toString() + " is valid client");
                    } catch (CertificateExpiredException | InvalidKeyException | NoSuchProviderException | SignatureException e) {
                        if (!acceptAllClient) {
                            throw new CertificateException(INVALID_CLIENT_X509_CERTIFICATE);
                        }
                    } catch (NoSuchAlgorithmException e) {
                        throw new CertificateException(UNIDENTIFIED_CLIENT_X509_CERTIFICATE_ALGORITHM);
                    }
                } else {
                    throw new CertificateException(UNIDENTIFIED_CLIENT_X509_CERTIFICATE);
                }
            }
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            X509Certificate[] issuers = getAcceptedIssuers();
            for (X509Certificate cert : chain) {
                if (cert != null) {
                    try {
                        cert.checkValidity();
                        for (X509Certificate issuer : issuers) {
                            if (cert.getSubjectDN().getName().compareTo(issuer.getSubjectDN().getName()) == 0) {
                                cert.verify(issuer.getPublicKey());
                                break;
                            }
                        }
                        Log.d(SDLVNCActivity.TAG, cert.toString() + " is valid server");
                    } catch (CertificateExpiredException | InvalidKeyException | NoSuchProviderException | SignatureException e) {
                        if (!acceptAllServer) {
                            throw new CertificateException(INVALID_SERVER_X509_CERTIFICATE);
                        }
                    } catch (NoSuchAlgorithmException e) {
                        throw new CertificateException(UNIDENTIFIED_SERVER_X509_CERTIFICATE_ALGORITHM);
                    }
                } else {
                    throw new CertificateException(UNIDENTIFIED_SERVER_X509_CERTIFICATE);
                }
            }
        }

        public X509Certificate[] getAcceptedIssuers() {
            Vector<X509Certificate> certificates = new Vector<>();
            try {
                KeyStore ks = KeyStore.getInstance(ANDROID_CA_STORE);
                if (ks != null) {
                    ks.load(null, null);
                    Enumeration aliases = ks.aliases();
                    while (aliases.hasMoreElements()) {
                        String alias = (String) aliases.nextElement();
                        X509Certificate cert = (X509Certificate) ks.getCertificate(alias);
                        certificates.add(cert);
                    }
                }
            } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
                e.printStackTrace();
            }

            return (X509Certificate[]) certificates.toArray();
        }
    }
}
