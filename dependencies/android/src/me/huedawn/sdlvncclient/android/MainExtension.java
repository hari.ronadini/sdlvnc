package me.huedawn.sdlvncclient.android;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import org.haxe.lime.GameActivity;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by rona on 27/07/16.
 */
public class MainExtension extends org.haxe.extension.Extension {


    static final String HOSTNAME = "hostname";
    static final String PASSWORD = "password";


    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static void apply(boolean acceptAllClient, boolean acceptAllServer) {

        TrustManager tm = new PrivateSSLSocketFactory.PrivateX509TrustManager(acceptAllClient, acceptAllServer);

        SSLContext context;
        try {
            context = SSLContext.getInstance("TLS");
            context.init(null, new TrustManager[]{tm}, null);
            SSLContext.setDefault(context);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }

    }

    public static void startVNCActivity(String hostname, String password) {

        Log.d(SDLVNCActivity.TAG, "Starting VNC Activity");
        Context context = GameActivity.getContext();
        Intent intent = new Intent(context, SDLVNCActivity.class);
        intent.putExtra(HOSTNAME, hostname);
        intent.putExtra(PASSWORD, password);
        context.startActivity(intent);

    }

    static {
        System.loadLibrary("sdlvncclient");
    }

    public static native void getEnv();

    public static native void startVNCClient(String hostname, String password);

}
