package me.huedawn.sdlvncclient.android;

import android.os.Bundle;
import android.util.Log;
import org.libsdl.app.SDLActivity;

/**
 * Created by rona on 28/07/16.
 */
public class SDLVNCActivity extends SDLActivity {


    public static final String TAG = "SDLVNC";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.loadLibrary("sdlvncclient");

        MainExtension.getEnv();

        Log.d(TAG, "SDLVNCActivity created");
    }

    public String getHostname() {
        return getIntent().getStringExtra(MainExtension.HOSTNAME);
    }

    public String getPassword() {
        return getIntent().getStringExtra(MainExtension.PASSWORD);
    }

}
