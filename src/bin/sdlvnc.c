/*
 This file is part of sdlvnc.

 sdlvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 sdlvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with sdlvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <pthread.h>
#include <errno.h>
#ifdef _DEBUG
#include <mcheck.h>
#endif
#include <sdlvnc_log.h>
#include <sdlvnc_handler.h>
#include "sdlvnc_icon.h"
#include "sdlvnc.h"

#define	SDLVNC_THREAD_ID		"SDLVNC_THREAD"

#define clipboard_cut_callback	GotXCutTextProc

typedef struct sdlvnc_data {
	rfbClient *client;
	SDL_Thread *client_thread;

	sdlvnc_size view_size;
} sdlvnc_data;

static sdlvnc_data *instance;

char *password_callback() {
	return getpass("Enter remote password: ");
}

sdlvnc_size view_size_callback() {
	return instance->view_size;
}

void view_resize_callback(int w, int h) {
	instance->view_size.w = w;
	instance->view_size.h = h;
}

static int client_func(void *data) {
	return sdlvnc_start_client((rfbClient **) data);
}

int main(int argc, char **args) {
	char *hostname;

#ifdef _DEBUG
	mtrace();
#endif

	if (argc < 2 || !strlen(args[1])) {
		sdlvnc_print_debug("Usage: sdlvnc <hostname[:port]>\n");
		return EXIT_FAILURE;
	}

	if (strlen(args[1])) {
		hostname = args[1];
	} else {
		sdlvnc_print_error("Invalid hostname: %s\n", hostname);
		return EXIT_FAILURE;
	}

	return sdlvnc_start(hostname, 1, -1, (void *) sdlvncicon256, 256,
			password_callback, NULL, NULL, view_size_callback,
			view_resize_callback, NULL);
}

int sdlvnc_start(const char *hostname, int use_sdl, int win_id, void *icon,
		int icon_dim, sdlvnc_password_callback password_callback,
		sdlvnc_paint_callback paint_buffer_callback,
		sdlvnc_paint_final_callback paint_final_buffer_callback,
		sdlvnc_size_callback view_size_callback,
		sdlvnc_resize_callback view_resize_callback,
		sdlvnc_clipcut_callback text_cut_callback) {
	int result;
	int client_status;

	result = EXIT_SUCCESS;

// Initialize data
	instance = malloc(sizeof(sdlvnc_data));
	if (!instance) {
		sdlvnc_print_error("Fail allocating instance\n");
		return EXIT_FAILURE;
	}
	instance->view_size.w = 800;
	instance->view_size.h = 600;

// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		sdlvnc_print_error("Unable to initialize SDL: %s\n", SDL_GetError());
		free(instance);
		return EXIT_FAILURE;
	}

// Initialize client
	instance->client = sdlvnc_init_client(hostname, 1, 0, icon, icon_dim,
			password_callback, paint_buffer_callback,
			paint_final_buffer_callback, view_size_callback,
			view_resize_callback, (GotXCutTextProc) text_cut_callback);
	if (!instance->client) {
		sdlvnc_print_error("Fail initiating client\n");
		free(instance);
		return EXIT_FAILURE;
	}

// Initialize thread
	instance->client_thread = SDL_CreateThread(&client_func, SDLVNC_THREAD_ID,
			&instance->client);
	if (!instance->client_thread) {
		sdlvnc_print_error("Fail initiating thread\n");
		sdlvnc_clear(instance->client);
		free(instance);
		result = EXIT_FAILURE;
	}

// main
	if (result != EXIT_FAILURE) {
		result = sdlvnc_start_sdl(instance->client);
	}

// Wait for client thread
	if (result != EXIT_FAILURE) {
		SDL_WaitThread(instance->client_thread, &client_status);

		result = client_status;
	}

// Cleanup
	if (instance)
		free(instance);

	SDL_Quit();

	printf("result: %d\n", result);

	return result;
}
