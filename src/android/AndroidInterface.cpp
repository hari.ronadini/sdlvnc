#include <jni.h>
#include "SDLvncviewer.h"
#include "SDLVNCLogger.h"
#include "AndroidInterface.h"


#ifdef __ANDROID__


#ifdef __cplusplus
extern "C"
{
#endif


static JNIEnv *gEnv;


const char* Java_me_huedawn_sdlvncclient_android_SDLVNCActivity_getHostname ();


JNIEXPORT void JNICALL Java_me_huedawn_sdlvncclient_android_MainExtension_getEnv (JNIEnv* env, jclass cls) {

  gEnv = env;

}


JNIEXPORT void JNICALL Java_me_huedawn_sdlvncclient_android_MainExtension_startVNCClient (JNIEnv* env, jclass cls, jstring hostname, jstring password) {

  jboolean isCopy;
  const char *hostnameLocal;
  const char *passwordLocal;

  print_debug((char*) "Initiating native call\n");

  hostnameLocal = env->GetStringUTFChars(hostname, &isCopy);
  passwordLocal = env->GetStringUTFChars(password, &isCopy);

  print_debug((char*) "Calling native method\n");

  start_client(hostnameLocal, passwordLocal);

  env->ReleaseStringUTFChars(hostname, hostnameLocal);
  env->ReleaseStringUTFChars(password, passwordLocal);

  print_debug((char*) "Done calling native method\n");

}


jclass getSDLVNCActivityObject () {

  jclass sdlVNCActivityClass;

  sdlVNCActivityClass = gEnv->FindClass("me/huedawn/sdlvncclient/android/SDLVNCActivity");
  return (jclass) gEnv->NewGlobalRef(sdlVNCActivityClass);

}

const char* Java_me_huedawn_sdlvncclient_android_SDLVNCActivity_getHostname () {

  jboolean isCopy;
  jclass sdlVNCActivityObject;
  jmethodID idGetHostname;
  jobject hostname;
  const char *hostnameLocal;

  if (gEnv == 0) {
    print_error((char*) "Call Java_me_huedawn_sdlvncclient_android_MainExtension_getEnv before use!\n");
    return 0;
  }

  // Getting the method
  sdlVNCActivityObject = getSDLVNCActivityObject();
  idGetHostname = gEnv->GetMethodID(sdlVNCActivityObject, "getHostname", "(V)Ljava/lang/String;");

  // Call the method
  hostname = gEnv->CallObjectMethod(sdlVNCActivityObject, idGetHostname);
  hostnameLocal = gEnv->GetStringUTFChars((jstring) hostname, &isCopy);

  return hostnameLocal;

}


const char* Java_me_huedawn_sdlvncclient_android_SDLVNCActivity_getPassword () {

  jboolean isCopy;
  jclass sdlVNCActivityObject;
  jmethodID idGetPassword;
  jobject password;
  const char *passwordLocal;

  if (gEnv == 0) {
    print_error((char*) "Call Java_me_huedawn_sdlvncclient_android_MainExtension_getEnv before use!\n");
    return 0;
  }

  // Getting the method
  sdlVNCActivityObject = getSDLVNCActivityObject();
  idGetPassword = gEnv->GetMethodID(sdlVNCActivityObject, "getPassword", "(V)Ljava/lang/String;");

  // Call the method
  password = gEnv->CallObjectMethod(sdlVNCActivityObject, idGetPassword);
  passwordLocal = gEnv->GetStringUTFChars((jstring) password, &isCopy);

  return passwordLocal;

}


#ifdef __cplusplus
}
#endif


#endif /* __ANDROID__ */
