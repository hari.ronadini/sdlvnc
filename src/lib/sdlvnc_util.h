/*
 This file is part of libsdlvnc.

 libsdlvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 libsdlvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SDLVNCUTIL_H__
#define __SDLVNCUTIL_H__

#include <time.h>
#include <rfb/rfbclient.h>

#define UNUSED(arg) (void)arg;

#ifdef __cplusplus
extern "C" {
#endif

long curr_time_in_milli_s(void);
unsigned int curr_time_in_micro_s(void);
unsigned int millistomicros(long millis);
long microstomillis(unsigned int micros);
struct timespec make_delay(unsigned int micros);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // __SDLVNCUTIL_H__
