/*
 This file is part of libsdlvnc.

 libsdlvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 libsdlvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include "config.h"
#include "sdlvnc_util.h"
#include "sdlvnc_log.h"
#include "sdlvnc_thread.h"
#include "sdlvnc_handler.h"
#include "sdlvnc_client.h"

#define SDLVNC_DATA 		"SDLVNC_DATA"	// id to data structure
#define SDLVNC_SIGNAL_WAIT	10				// time window for getting signal in microseconds
#define SDLVNC_CLIENT_WAIT	1000			// time window for getting server messages in microseconds
#define SDLVNC_SDL_WAIT		1				// time window for getting SDL events in milliseconds

typedef struct sdlvnc_client {
	int fullscreen;
	int viewOnly;
	int typeSize;
	int quit;
	int cursorX;
	int cursorY;
	int cursorW;
	int cursorH;
	int initialized;

	int use_sdl;

	sdlvnc_password_callback password_callback;
	sdlvnc_paint_callback paint_callback;
	sdlvnc_paint_final_callback paint_final_callback;
	sdlvnc_size_callback view_size_callback;
	sdlvnc_resize_callback view_resize_callback;

	void *frameUpdate;

	void *icon;
	int icon_dim;
} sdlvnc_data;

static int lib_quit = 0;

void sdlvnc_clear(rfbClient *cl);

int sdlvnc_is_view_only(rfbClient *client) {
	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);
	return data->viewOnly;
}

int sdlvnc_is_big_endian(rfbClient *client) {
	return client->si.format.bigEndian;
}

int sdlvnc_is_fullscreen(rfbClient *client) {
	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);
	return data->fullscreen;
}

rfbBool sdlvnc_new_fb(rfbClient *client) {
	if (client->frameBuffer)
		free(client->frameBuffer);

	size_t size;
	int byte_size = client->format.bitsPerPixel / 8;

	size = client->width * client->height * byte_size;
	sdlvnc_print_debug("frameBuffer: %d %d\n", byte_size, size);

	if (client->frameBuffer)
		free(client->frameBuffer);
	client->frameBuffer = malloc(size);
	assert(client->frameBuffer);

	return TRUE;
}

rfbBool sdlvnc_update_client(rfbClient *client) {
	client->updateRect.x = 0;
	client->updateRect.y = 0;
	client->updateRect.w = client->width;
	client->updateRect.h = client->height;
	client->format.bitsPerPixel = client->si.format.bitsPerPixel;
	client->format.depth = client->si.format.depth;
	client->format.bigEndian = client->si.format.bigEndian;
	client->format.trueColour = client->si.format.trueColour;
	client->format.redMax = client->si.format.redMax;
	client->format.greenMax = client->si.format.greenMax;
	client->format.blueMax = client->si.format.blueMax;
	client->format.redShift = client->si.format.redShift;
	client->format.greenShift = client->si.format.greenShift;
	client->format.blueShift = client->si.format.blueShift;

	if (!SetFormatAndEncodings(client)) {
		sdlvnc_print_error("Fail to request change to server\n");
		sdlvnc_quit_client(client, EXIT_FAILURE);
		return FALSE;
	}

	return TRUE;
}

rfbBool sdlvnc_mallocfb(rfbClient *client) {
	int scale = 1;

	sdlvnc_print_debug("View: %d %dx%d\n", scale, client->width,
			client->height);

	sdlvnc_print_debug("View: client bpp %d\n", client->format.bitsPerPixel);
	sdlvnc_print_debug("Server: %dx%d bpp %d depth %d endian %s\n",
			client->si.framebufferWidth, client->si.framebufferHeight,
			client->si.format.bitsPerPixel, client->si.format.depth,
			(client->si.format.bigEndian ? "TRUE" : "FALSE"));

	if (!sdlvnc_update_client(client)) {
		sdlvnc_print_error("Fail updating client!\n");
		return FALSE;
	}

	if (!sdlvnc_new_fb(client)) {
		sdlvnc_print_error("Fail allocating framebuffer\n");
		return FALSE;
	}

	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);
	(*data->view_resize_callback)(client->width, client->height);

	sdlvnc_print_debug("Done resizing\n");

	return TRUE;
}

void sdlvnc_paint_buffer(rfbClient* client, void* frame, int x, int y, int w,
		int h) {
	sdlvnc_data *data;
	data = rfbClientGetClientData(client, SDLVNC_DATA);
	(*data->paint_callback)(frame, x, y, w, h);
}

void sdlvnc_paint(rfbClient *client, int x, int y, int w, int h) {
	void *frame;
	int pitch;

	frame = sdlvnc_fbcpy(client, 4, x, y, w, h, frame, &pitch);

	sdlvnc_paint_buffer(client, frame, x, y, w, h);

	free(frame);
}

void sdlvnc_paint_buffer_final(rfbClient* client, void *buffer) {
	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);
	(*data->paint_final_callback)(buffer);
}

void sdlvnc_paint_final(rfbClient *client) {
	sdlvnc_paint_buffer_final(client, client->frameBuffer);
}

void kbd_leds(rfbClient *cl, int value, int pad) {
	UNUSED (cl)
	UNUSED (pad)
	/* note: pad is for future expansion 0=unused */
	sdlvnc_print_error("Led State= 0x%02X\n", value);
}

/* trivial support for textchat */
void text_chat(rfbClient *cl, int value, char *text) {
	switch (value) {
	case rfbTextChatOpen:
		sdlvnc_print_error("TextChat: We should open a textchat window!\n");
		TextChatOpen(cl);
		break;
	case rfbTextChatClose:
		sdlvnc_print_error("TextChat: We should close our window!\n");
		break;
	case rfbTextChatFinished:
		sdlvnc_print_error("TextChat: We should close our window!\n");
		break;
	default:
		sdlvnc_print_error("TextChat: Received \"%s\"\n", text);
		break;
	}
}

void sdlvnc_clear(rfbClient *client) {
	assert(client);
	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);
	assert(data);

	data->frameUpdate = realloc(data->frameUpdate, 0);
	if (data->frameUpdate)
		free(data->frameUpdate);

	free(data);
	sdlvnc_clear_sdl(client);
	sdlvnc_clear_threading(client);
	if (client->frameBuffer)
		free(client->frameBuffer);
	rfbClientCleanup(client);
}

char *get_password(rfbClient *client) {
	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);
	char *password = (*data->password_callback)();
	return password;
}

void at_signal(int code) {
	lib_quit = code;
}

void at_exit(void) {
	at_signal (EXIT_SUCCESS);
}

void sdlvnc_cursor_lock(rfbClient *client, int x, int y, int w, int h) {
	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);
	data->cursorX = x;
	data->cursorY = y;
	data->cursorW = w;
	data->cursorH = h;
	size_t next_line;
	int byte_size = client->format.bitsPerPixel / 8;
	size_t line_size = client->width * byte_size;
	size_t cursor_line_size = w * byte_size;
	int i;

	for (i = 0; i < h; i++) {
		next_line = line_size * (y + i) + byte_size * x;
		mlock(client->frameBuffer + next_line, cursor_line_size);
	}
}

void sdlvnc_cursor_unlock(rfbClient *client) {
	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);
	int x = data->cursorX;
	int y = data->cursorY;
	int w = data->cursorW;
	int h = data->cursorH;
	size_t next_line;
	int byte_size = client->format.bitsPerPixel / 8;
	size_t line_size = client->width * byte_size;
	size_t cursor_line_size = w * byte_size;
	int i;

	for (i = 0; i < h; i++) {
		next_line = line_size * (y + i) + byte_size * x;
		munlock(client->frameBuffer + next_line, cursor_line_size);
	}
}

void sdlvnc_free_args(int argc, char** args) {
	int i;
	char* arg;
	for (i = 0; i < argc; i++) {
		arg = args[i];
		free(arg);
	}
	free(args);
}

rfbClient *sdlvnc_init_client(const char *host, int use_sdl, Uint32 win_id,
		void *icon, int icon_dim, sdlvnc_password_callback password_callback,
		sdlvnc_paint_callback paint_callback,
		sdlvnc_paint_final_callback paint_final_callback,
		sdlvnc_size_callback view_size_callback,
		sdlvnc_resize_callback view_resize_callback,
		GotXCutTextProc text_cut_callback) {
	char *defargs[] = { "-listennofork", "-compress", "9", "-ssl" };
	char **args;
	int argc = 5;
	int i;
	rfbClient *client;
	sdlvnc_data *data;
	int arglen = 0;
	int maxarglen = 0;

	// arguments
	// Assign parameters
	args = malloc(sizeof(char *) * argc);
	// Calculate max length of array item
	for (i = 0; i < argc - 1; i++) {
		arglen = strlen(defargs[i]) + 1;
		if (maxarglen < arglen)
			maxarglen = arglen;
	}
	arglen = strlen(host) + 1;
	if (maxarglen < arglen)
		maxarglen = arglen;
	// Copying default arguments
	for (i = 0; i < argc - 1; i++) {
		args[i] = malloc((strlen(defargs[i]) + 1) * sizeof(char));
		args[i] = strcpy(args[i], defargs[i]);
	}
	// Cut protocol and copy hostname
	if (strstr(host, "vnc://")) {
		args[argc - 1] = malloc((strlen(host + 6) + 1) * sizeof(char));
		args[argc - 1] = strcpy(args[argc - 1], host + 6);
	} else {
		args[argc - 1] = malloc((strlen(host) + 1) * sizeof(char));
		args[argc - 1] = strcpy(args[argc - 1], host);
	}
	for (i = 0; i < argc; i++) {
		sdlvnc_print_debug("arg[%d]: %s\n", i, args[i]);
	}
	// arguments

	assert(view_size_callback);
	assert(view_resize_callback);

	sdlvnc_print_debug("Entering initializer\n");

	data = malloc(sizeof(sdlvnc_data));
	data->initialized = 0;
	data->quit = 0;
	data->use_sdl = use_sdl;
	data->password_callback = password_callback;
	data->paint_callback = paint_callback;
	data->paint_final_callback = paint_final_callback;
	data->view_size_callback = view_size_callback;
	data->view_resize_callback = view_resize_callback;
	data->frameUpdate = NULL;
	data->icon = icon;
	data->icon_dim = icon_dim;

	// TODO Make application interface to set this
	data->viewOnly = 0;
	data->fullscreen = 0;

	/* 16-bit: client=rfbGetClient(5,3,2); */
	client = rfbGetClient(8, 3, 4);
	client->canHandleNewFBSize = FALSE; // TODO Server resize
	client->GetPassword = get_password;
	client->HandleKeyboardLedState = kbd_leds;
	client->HandleTextChat = text_chat;
	client->GotXCutText = text_cut_callback;
	client->listenPort = LISTEN_PORT_OFFSET;
	client->listen6Port = LISTEN_PORT_OFFSET;

	if (data->use_sdl) {
		client->MallocFrameBuffer = sdlvnc_mallocfb_sdl;
//		client->GotCursorShape = sdlvnc_cursor_set_sdl; // Must be MSB Format
		client->GotFrameBufferUpdate = sdlvnc_paint_sdl;
		client->FinishedFrameBufferUpdate = sdlvnc_paint_final_sdl;
		client->SoftCursorLockArea = sdlvnc_cursor_lock_sdl;
		client->SoftCursorUnlockScreen = sdlvnc_cursor_unlock_sdl;
		sdlvnc_set_winid_sdl(client, win_id);
	} else {
		client->MallocFrameBuffer = sdlvnc_mallocfb;
//		client->GotCursorShape = accept_new_remote_cursor; // Must be MSB Format
		client->GotFrameBufferUpdate = sdlvnc_paint;
		client->FinishedFrameBufferUpdate = sdlvnc_paint_final;
		client->SoftCursorLockArea = sdlvnc_cursor_lock;
		client->SoftCursorUnlockScreen = sdlvnc_cursor_unlock;
	}

	rfbClientSetClientData(client, SDLVNC_DATA, data);

	sdlvnc_print_debug("Initializing VNC\n");

	if (!client || !rfbInitClient(client, &argc, args)) {
		sdlvnc_print_debug("Client initialization failed!\n");

		sdlvnc_free_args(argc, args);

		return NULL;
	}

	sdlvnc_free_args(argc, args);

	data->initialized = 1;

	sdlvnc_print_debug("Initialized VNC\n");

	return client;
}

int sdlvnc_start_client(rfbClient **client) {
	assert(client);
	assert(*client);
	sdlvnc_data *data = rfbClientGetClientData(*client, SDLVNC_DATA);
	assert(data);
	int result = 0;
	int waitRes = 0;
	int quit = 0;

	sdlvnc_print_debug("sdlvnc_start_client loop\n");

	while (quit == 0 && lib_quit == 0) {
		if (*client) {
			waitRes = WaitForMessage(*client, SDLVNC_CLIENT_WAIT);
		}

		if (waitRes == -4) {
			// Interrupted system call. Problem with neko vm.
			sdlvnc_print_error("Wait fail! Error: %d %d %s\n", waitRes, errno,
					strerror(errno));
			result = EXIT_FAILURE;
			break;
		} else if (waitRes < 0) {
			// Other error.
			sdlvnc_print_error("Wait fail! Error: %d %d %s\n", waitRes, errno,
					strerror(errno));
			result = EXIT_FAILURE;
			break;
		} else if (waitRes >= 0 && *client
				&& !HandleRFBServerMessage(*client)) {
			sdlvnc_print_debug("HandleRFBServerMessage fail! Error: %d %d %s\n",
					waitRes, errno, strerror(errno));
			if (waitRes == 0)
				result = EXIT_SUCCESS;
			else
				result = EXIT_FAILURE;
			break;
		}

		// Check for signals
		quit = sdlvnc_get_quit_timed(*client, SDLVNC_SIGNAL_WAIT);
	}

	if (*client) {
		sdlvnc_clear(*client);
	}

	result = sdlvnc_translate_quit(quit);

	sdlvnc_print_debug("Leaving sdlvnc_start_client\n");

	return result;
}

int sdlvnc_is_initialized(rfbClient *client) {
	sdlvnc_thread_data *thread = sdlvnc_get_threading(client);
	SDLVNC_LOCK (thread)

	if (!client)
		return 0;
	sdlvnc_data *data = rfbClientGetClientData(client, SDLVNC_DATA);

	SDLVNC_UNLOCK (thread)

	return data->initialized;
}

/*
 arguments:
 code:
 0   = continue
 1   = EXIT_SUCCESS
 -1  = EXIT_FAILURE
 */
void sdlvnc_quit_client(rfbClient *client, int code) {
	if (!client)
		return;

	sdlvnc_data *data;

	sdlvnc_thread_data *thread = sdlvnc_get_threading(client);
	SDLVNC_LOCK (thread)

	data = rfbClientGetClientData(client, SDLVNC_DATA);
	if (!data)
		return;
	if (code == EXIT_SUCCESS)
		data->quit = 1;
	else if (code == EXIT_FAILURE)
		data->quit = -1;
	else
		data->quit = code;

	SDLVNC_SIGNAL (thread)
	SDLVNC_UNLOCK (thread)
}

void pushWindowResizedEvent() // TODO Move to sdlvnc_handler and rename to sdlvnc_window_resize_sdl()
{
	SDL_Event event;
	event.type = SDL_WINDOWEVENT_RESIZED;

	if (SDL_PushEvent(&event) != 1) {
		sdlvnc_print_debug("Event SDL_WINDOWEVENT_RESIZED fail: %s\n",
				SDL_GetError());
	}
}

sdlvnc_size sdlvnc_get_client_size(rfbClient *client) {
	sdlvnc_thread_data *thread = sdlvnc_get_threading(client);
	SDLVNC_LOCK (thread)

	sdlvnc_size size;
	if (!sdlvnc_is_initialized(client))
		return size;
	size = (sdlvnc_size ) { client->width, client->height };

	SDLVNC_UNLOCK (thread)

	return size;
}

void sdlvnc_paste_text(rfbClient *client, const char *text) {
	sdlvnc_thread_data *thread = sdlvnc_get_threading(client);
	SDLVNC_LOCK (thread)

	int i, len;

	len = strlen(text);
	for (i = 0; i < len; i++) {
		SendKeyEvent(client, text[i], TRUE);
		SendKeyEvent(client, text[i], FALSE);
	}

	SDLVNC_UNLOCK (thread)
}

/*
 returns:
 0   = continue
 1   = EXIT_SUCCESS
 -1  = EXIT_FAILURE
 */
int sdlvnc_get_quit_timed(rfbClient *client, unsigned int micros) {
	if (!client)
		return 1;

	int quit;
	struct timespec time;
	sdlvnc_data *data;

	time = make_delay(micros);

	sdlvnc_thread_data *thread = sdlvnc_get_threading(client);
	SDLVNC_LOCK (thread)
	SDLVNC_SIG_WAIT_TIMED(data, &time)
	data = rfbClientGetClientData(client, SDLVNC_DATA);
	if (!data)
		return 1;
	quit = data->quit;
	SDLVNC_UNLOCK (thread)

	return quit;
}

/*
 returns:
 0   = continue
 1   = EXIT_SUCCESS
 -1  = EXIT_FAILURE
 */
int sdlvnc_get_quit(rfbClient *client) {
	if (!client)
		return 1;

	int quit;
	sdlvnc_data *data;
	sdlvnc_thread_data *thread = sdlvnc_get_threading(client);
	SDLVNC_LOCK (thread)
	SDLVNC_SIG_WAIT (data)
	data = rfbClientGetClientData(client, SDLVNC_DATA);
	quit = data->quit;
	SDLVNC_UNLOCK (thread)

	return quit;
}

int sdlvnc_translate_quit(int quit) {
	int result;

	if (quit == 1)
		result = EXIT_SUCCESS;
	else if (quit == -1)
		result = EXIT_FAILURE;
	else
		result = quit;

	return result;
}

void* sdlvnc_fbcpy(rfbClient* client, int bytesPerPixel, int x, int y, int w,
		int h, void* update, int* pitch) {
	// To copy updated buffer
	int i;
	size_t client_pitch, update_pitch, client_line, updated_line;

	// Update only the changed area, copy the area to a new frame-buffer
	client_pitch = bytesPerPixel * client->width; // This using the entire frame-buffer
	update_pitch = bytesPerPixel * w; // This is using only the updated area

	// Check allocation
	update = malloc(update_pitch * h);

	// Copy the updated area
	for (i = 0; i < h; i++) {
		client_line = client_pitch * (y + i) + bytesPerPixel * x; // y+x
		updated_line = update_pitch * i; // y
		memcpy(update + updated_line, client->frameBuffer + client_line,
				update_pitch); // update<-client(update)
	}

	*pitch = update_pitch;

	return update;
}

sdlvnc_size sdlvnc_get_size(rfbClient *client) {
	sdlvnc_data *data;
	data = rfbClientGetClientData(client, SDLVNC_DATA);
	return (*data->view_size_callback)();
}

void sdlvnc_on_resize(rfbClient *client, int w, int h) {
	sdlvnc_data *data;
	data = rfbClientGetClientData(client, SDLVNC_DATA);
	(*data->view_resize_callback)(w, h);
}

void * sdlvnc_get_icon(rfbClient *client, void *icon, int *dim) {
	sdlvnc_data *data;
	data = rfbClientGetClientData(client, SDLVNC_DATA);
	*dim = data->icon_dim;
	return data->icon;
}
