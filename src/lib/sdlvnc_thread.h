/*
    This file is part of libsdlvnc.

    libsdlvnc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libsdlvnc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SDLVNCTHREAD_H__
#define __SDLVNCTHREAD_H__

#define _GNU_SOURCE
#include <pthread.h>
#include <rfb/rfbclient.h>

#define SDLVNC_THREADING_DATA \
    "sdlvnc_thread_data"
#define SDLVNC_LOCK(data) \
    UNUSED(data)
#define SDLVNC_UNLOCK(data) \
    UNUSED(data)
#define SDLVNC_SIGNAL(data) \
    UNUSED(data)
#define SDLVNC_SIG_WAIT(data) \
    {                         \
        UNUSED(data)          \
    }
#define SDLVNC_SIG_WAIT_TIMED(data, time) \
    {                                     \
        UNUSED(data)                      \
        UNUSED(time)                      \
    }

typedef struct sdlvnc_thread_data sdlvnc_thread_data;

#ifdef __cplusplus
extern "C" {
#endif

sdlvnc_thread_data *sdlvnc_get_threading(rfbClient *client);
void sdlvnc_set_threading(rfbClient *client, sdlvnc_thread_data *data);
void sdlvnc_clear_threading(rfbClient *client);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // __SDLVNCTHREAD_H__