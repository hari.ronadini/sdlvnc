#include <search.h>
#include <rfb/rfbclient.h>
#include <sys/mman.h>
#include "sdlvnc_keyboard_queue.h"

#define SDLVNC_KEYQUEUE "keyqueue"

typedef struct rfbkey_element {
    struct rfbkey_element *forward;
    struct rfbkey_element *backward;
    sdlvnc_keyboard_data *data;
} rfbkey_element;

rfbkey_element *
new_element(void)
{
    rfbkey_element *e;

    e = malloc(sizeof(rfbkey_element));
    if (e == NULL) {
        rfbClientErr("new_element() failed\n");
    }
    return e;
}

sdlvnc_keyboard_data *
new_data(void)
{
    sdlvnc_keyboard_data *d;

    d = malloc(sizeof(sdlvnc_keyboard_data));
    if (d == NULL) {
        rfbClientErr("new_data() failed\n");
    }
    return d;
}

void rfbkey_queue(rfbClient *client, rfbKeySym key, rfbBool shiftdown)
{
    mlock(client, sizeof(rfbClient));
    rfbkey_element *last = rfbClientGetClientData(client, SDLVNC_KEYQUEUE);
    rfbkey_element *newel = new_element();
    newel->data = new_data();
    newel->data->key = key;
    newel->data->shiftdown = shiftdown;
    insque(newel, last);
    last = newel;
    rfbClientSetClientData(client, SDLVNC_KEYQUEUE, last);
    munlock(client, sizeof(rfbClient));
}

void rfbkey_pop(rfbClient *client)
{
    mlock(client, sizeof(rfbClient));
    rfbkey_element *el = rfbClientGetClientData(client, SDLVNC_KEYQUEUE);
    rfbkey_element *last = el;
    rfbkey_element *first;
    if (el == NULL)
        return;
    while (el != NULL)
    {
        first = el;
        el = el->forward;
    }
    sdlvnc_keyboard_data *data = first->data;
    SendKeyEvent(client, data->key, data->shiftdown);
    free(data);
    remque(first);
    if (first) {
        free(first);
        first = NULL;
    }
    if (last == first)
        rfbClientSetClientData(client, SDLVNC_KEYQUEUE, NULL);
    munlock(client, sizeof(rfbClient));
}
