/*
    This file is part of libsdlvnc.

    libsdlvnc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libsdlvnc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "x11.h"


Display *x11_opendisplay(char *name) {
    return XOpenDisplay(name);
}


Window *x11_winlist (Display *disp, unsigned long *len) {

    int form;
    unsigned long remain;
    unsigned char *list;
    Atom prop, type;

    if (!disp)
        disp = x11_opendisplay(NULL);

    prop = XInternAtom(disp,"_NET_CLIENT_LIST",True);

    errno = 0;
    if (XGetWindowProperty(disp,XDefaultRootWindow(disp),prop,0,1024,False,XA_WINDOW,
                &type,&form,len,&remain,&list) != Success) {
        perror("x11_winlist() -- XGetWindowProperty");
        return 0;
    }

    return (Window*)list;
}


char *x11_winname (Display *disp, Window win) {

    int form;
    unsigned long remain, len;
    unsigned char *list;
    Atom prop, type;

    if (win == (Window)NULL)
        return NULL;

    if (!disp)
        disp = x11_opendisplay(NULL);

    prop = XInternAtom(disp,"WM_NAME",False);

    errno = 0;
    if (XGetWindowProperty(disp,win,prop,0,1024,False,XA_STRING,
                &type,&form,&len,&remain,&list) != Success) {
        perror("x11_clsname() -- XGetWindowProperty");
        return NULL;
    }

    return (char*)list;
}


char *x11_clsname (Display *disp, Window win) {

    int form;
    unsigned long remain, len;
    unsigned char *list;
    Atom prop, type;

    if (win == (Window)NULL)
        return NULL;

    if (!disp)
        disp = x11_opendisplay(NULL);

    prop = XInternAtom(disp,"WM_CLASS",False);

    errno = 0;
    if (XGetWindowProperty(disp,win,prop,0,1024,False,XA_STRING,
                &type,&form,&len,&remain,&list) != Success) {
        perror("x11_clsname() -- XGetWindowProperty");
        return NULL;
    }

    return (char*)list;
}

XWMHints *x11_xwmhints (Display *disp, Window win) {

    if (win == (Window)NULL)
        return NULL;

    if (!disp)
        disp = x11_opendisplay(NULL);

    return XGetWMHints(disp, win);
}

char *x11_xwmhints_char (Display *disp, Window win) {

    char *hints_char;
    int hints_len;
    XWMHints *hints;

    if (win == (Window)NULL)
        return NULL;

    if (!disp)
        disp = x11_opendisplay(NULL);

    hints = x11_xwmhints(disp, win);

    errno = 0;
    if (!hints) {
        perror("x11_xwmhints_char() -- x11_xwmhints");
        return NULL;
    }

    hints_len = snprintf(hints_char, 0, "InputHint=%d|StateHint=%d|IconPixmapHint=%d|IconWindowHint=%d|IconPositionHint=%d|IconMaskHint=%d|WindowGroupHint=%d|UrgencyHint=%d", \
        (hints->flags & (1L << 0)? 1: 0), \
        (hints->flags & (1L << 1)? 1: 0), \
        (hints->flags & (1L << 2)? 1: 0), \
        (hints->flags & (1L << 3)? 1: 0), \
        (hints->flags & (1L << 4)? 1: 0), \
        (hints->flags & (1L << 5)? 1: 0), \
        (hints->flags & (1L << 6)? 1: 0), \
        (hints->flags & (1L << 8)? 1: 0)) + 1;

    hints_char = (char*) malloc(hints_len * sizeof(char));
    hints_len = snprintf(hints_char, hints_len, "InputHint=%d|StateHint=%d|IconPixmapHint=%d|IconWindowHint=%d|IconPositionHint=%d|IconMaskHint=%d|WindowGroupHint=%d|UrgencyHint=%d", \
        (hints->flags & (1L << 0)? 1: 0), \
        (hints->flags & (1L << 1)? 1: 0), \
        (hints->flags & (1L << 2)? 1: 0), \
        (hints->flags & (1L << 3)? 1: 0), \
        (hints->flags & (1L << 4)? 1: 0), \
        (hints->flags & (1L << 5)? 1: 0), \
        (hints->flags & (1L << 6)? 1: 0), \
        (hints->flags & (1L << 8)? 1: 0));

    return hints_char;
}

Window x11_find_win (Display *disp, const char* search, int flags) {

    int i;
    char *name;
    XWMHints *hints;
    unsigned long len;
    Window *list;
    char *hints_char;

    if (!disp)
        disp = x11_opendisplay(NULL);

    list = (Window*)x11_winlist(disp,&len);

    if (!list) {
        perror("x11_find_win() -- x11_winlist");
        return (Window)NULL;
    }

    for (i=0;i<(int)len;i++) {
        name = x11_clsname(disp,list[i]);
        if (name) {
            hints = x11_xwmhints(disp,list[i]);
            hints_char = x11_xwmhints_char(disp,list[i]);
            printf("(%s) %s\n",name,hints_char);
            if (name && strcmp(search,name) == 0 &&
                ((hints->flags & flags) == flags)) {
                free(name);
                free(hints);
                return (Window)list[i];
            }
            free(name);
            free(hints);
        }
    }

    return (Window)NULL;

}

Window x11_find_win_no_flags (Display *disp, const char* search) {

    int i;
    char *name;
    XWMHints *hints;
    unsigned long len;
    Window *list;
    char *hints_char;

    if (!disp)
        disp = x11_opendisplay(NULL);

    list = (Window*)x11_winlist(disp,&len);

    for (i=0;i<(int)len;i++) {
        name = x11_clsname(disp,list[i]);
        if (name) {
            hints = x11_xwmhints(disp,list[i]);
            hints_char = x11_xwmhints_char(disp,list[i]);
            printf("(%s) %s\n",name,hints_char);
            if (name && strcmp(search,name) == 0) {
                free(name);
                free(hints);
                return (Window)list[i];
            }
            free(name);
            free(hints);
        }
     }

    return (Window)NULL;

}
