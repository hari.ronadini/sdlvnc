/*
    This file is part of libsdlvnc.

    libsdlvnc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libsdlvnc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "x11.h"


int main(int argc, char *argv[]) {
    int i;
    unsigned long len;
    Display *disp = x11_opendisplay(NULL);
    Window *list;
    char *name;
    char *hints_char;
    int flags;
    Window found;

    if (!disp) {
        puts("no display!");
        return -1;
    }

    list = (Window*)x11_winlist(disp,&len);

    for (i=0;i<(int)len;i++) {
        name = x11_winname(disp,list[i]);
        if (name) {
            hints_char = x11_xwmhints_char(disp,list[i]);
            printf("(%s) %s\n",name,hints_char);
            free(name);
            free(hints_char);
        }
    }

    if (argc == 3) {

            flags = atoi(argv[2]);
            found = (Window)x11_find_win(NULL,argv[1],flags);
            name = x11_winname(disp,found);
            if (name && !strcmp(name,"(null)")) {
                hints_char = x11_xwmhints_char(disp,found);
                printf("\n Found (%s) %s\n",name,hints_char);
                free(name);
                free(hints_char);
            }

    } else {

        printf("\nUsage: %s <WinClassName> <flags>\n\n", argv[0]);

    }
    XFree(list);

    XCloseDisplay(disp);
    return 0;
}

