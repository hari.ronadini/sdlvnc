/*
 This file is part of libsdlvnc.

 libsdlvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 libsdlvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SDLVNCVIEWER_H
#define SDLVNCVIEWER_H

#include <rfb/rfbclient.h>
#include <SDL2/SDL.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct sdlvnc_size {
	int w, h;
} sdlvnc_size;

// Application required definition
typedef char *(*sdlvnc_password_callback)();
typedef void (*sdlvnc_paint_callback)(unsigned char *, int, int, int, int);
typedef void (*sdlvnc_paint_final_callback)(unsigned char *);
typedef sdlvnc_size (*sdlvnc_size_callback)();
typedef void (*sdlvnc_resize_callback)(int w, int h);

rfbClient *sdlvnc_init_client(const char *host, int use_sdl, Uint32 win_id,
		void *icon, int icon_dim, sdlvnc_password_callback password_callback,
		sdlvnc_paint_callback paint_buffer_callback,
		sdlvnc_paint_final_callback paint_final_buffer_callback,
		sdlvnc_size_callback view_size_callback,
		sdlvnc_resize_callback view_resize_callback,
		GotXCutTextProc text_cut_callback);
sdlvnc_size sdlvnc_get_size(rfbClient *client);
void sdlvnc_on_resize(rfbClient *client, int w, int h);
void sdlvnc_clear(rfbClient *client);
int sdlvnc_start_client(rfbClient **client);
rfbBool sdlvnc_update_client(rfbClient *client);
int sdlvnc_is_big_endian(rfbClient *client);
int sdlvnc_is_initialized(rfbClient *client);
void sdlvnc_quit_client(rfbClient *client, int code);
int sdlvnc_is_view_only(rfbClient *client);
int sdlvnc_is_fullscreen(rfbClient *client);
sdlvnc_size sdlvnc_get_client_size(rfbClient *client);
void sdlvnc_paste_text(rfbClient *client, const char *text);
int sdlvnc_get_quit_timed(rfbClient *client, unsigned int micros);
int sdlvnc_get_quit(rfbClient *client);
int sdlvnc_translate_quit(int quit);
void* sdlvnc_fbcpy(rfbClient* client, int bytesPerPixel, int x, int y, int w,
		int h, void* update, int* pitch);
void sdlvnc_paint_buffer(rfbClient* client, void* frame, int x, int y, int w,
		int h);
void sdlvnc_paint_buffer_final(rfbClient* client, void *buffer);
void * sdlvnc_get_icon(rfbClient *client, void *icon, int *dim);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SDLVNCVIEWER_H */
