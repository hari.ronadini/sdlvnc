/*
 This file is part of libsdlvnc.

 libsdlvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 libsdlvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SDLVNCHANDLER_H__
#define __SDLVNCHANDLER_H__

#include <SDL2/SDL.h>
#include "sdlvnc_client.h"

typedef void (*sdlvnc_win_id_cb)();

#ifdef __cplusplus
extern "C" {
#endif

rfbBool sdlvnc_mallocfb_sdl(rfbClient *client);
void sdlvnc_paint_sdl(rfbClient *client, int x, int y, int w, int h);
void sdlvnc_paint_final_sdl(rfbClient *client);
int sdlvnc_event_sdl(rfbClient *client, long millis);
int sdlvnc_start_sdl(rfbClient *client);
void *sdlvnc_create_sdl();
void sdlvnc_clear_sdl(rfbClient *client);
void sdlvnc_cursor_set_sdl(rfbClient *client, int xhot, int yhot, int width,
		int height, int bytesPerPixel);
void sdlvnc_set_winid_sdl(rfbClient *client, Uint32 id);
void sdlvnc_cursor_lock_sdl(rfbClient *client, int x, int y, int w, int h);
void sdlvnc_cursor_unlock_sdl(rfbClient *client);
void sdlvnc_set_view_size_sdl(rfbClient *client, sdlvnc_size size);
void sdlvnc_set_icon_sdl(rfbClient *client, void *icon, int dim);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // __SDLVNCHANDLER_H__
