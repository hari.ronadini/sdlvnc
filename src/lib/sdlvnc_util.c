/*
 This file is part of libsdlvnc.

 libsdlvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 libsdlvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <string.h>
#include "sdlvnc_util.h"

long curr_time_in_milli_s(void) {
	long millis; // Milliseconds
	struct timespec spec;

	clock_gettime(CLOCK_REALTIME, &spec);

	millis = round(spec.tv_nsec * 1.0e6); // Convert nanoseconds to milliseconds

	return millis;
}

unsigned int curr_time_in_micro_s(void) {
	unsigned int micros; // Microseconds
	struct timespec spec;

	clock_gettime(CLOCK_REALTIME, &spec);

	micros = round(spec.tv_nsec * 0.001); // Convert nanoseconds to milliseconds

	return micros;
}

unsigned int millistomicros(long millis) {
	return millis * 1000;
}

long microstomillis(unsigned int micros) {
	return micros * 0.001;
}

struct timespec make_delay(unsigned int micros) {
	struct timespec spec;

	clock_gettime(CLOCK_REALTIME, &spec);
	spec.tv_nsec += micros * 1000;

	return spec;
}
