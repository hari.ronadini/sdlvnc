/*
    This file is part of libsdlvnc.

    libsdlvnc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libsdlvnc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef __ANDROID__
#include <android/log.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <rfb/rfbclient.h>
#include "sdlvnc_log.h"

const char INTERRUPTED_SYSTEM_CALL_MESSAGE[] = "Interrupted system call";
const char APP_NAME[] = "sdlvnc";

int spurious_message(const char *needle, const char *format, ...);
void log_to_file(const char *format, ...);
char *alloc_printf(const char *format, ...);

void sdlvnc_print_debug(const char *format, ...)
{
  va_list args;

  if (!format || format == 0x0)
    return;

  va_start(args, format);
  if (spurious_message(INTERRUPTED_SYSTEM_CALL_MESSAGE, format, args))
  {
    return;
  }
  va_end(args);

#ifdef __ANDROID__
  va_start(args, format);
  __android_log_vprint(ANDROID_LOG_DEBUG, APP_NAME, format, args);
  va_end(args);
#else
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
#endif
#ifdef LOG_TO_FILE
  va_start(args, format);
  log_to_file(format, args);
  va_end(args);
#endif
}

void sdlvnc_print_error(char *format, ...)
{
  va_list args;

  if (!format || format == 0x0)
    return;

  va_start(args, format);
  if (spurious_message(INTERRUPTED_SYSTEM_CALL_MESSAGE, format, args))
  {
    return;
  }
  va_end(args);

#ifdef __ANDROID__
  va_start(args, format);
  __android_log_vprint(ANDROID_LOG_ERROR, APP_NAME, format, args);
  va_end(args);
#else
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
#endif
#ifdef LOG_TO_FILE
  va_start(args, format);
  log_to_file(format, args);
  va_end(args);
#endif
}

void log_to_file(const char *format, ...)
{
  FILE *logfile;
  static char *logfile_str = 0;
  va_list args;
  char buf[255];
  time_t log_clock;

  if (!format || format == 0x0)
    return;

  if (!rfbEnableClientLogging)
    return;

  if (logfile_str == 0)
  {
    logfile_str = getenv("VNCLOG");
    if (logfile_str == 0)
      logfile_str = "vnc.log";
  }

  logfile = fopen(logfile_str, "a");

  va_start(args, format);

  // Time tag
  time(&log_clock);
  strftime(buf, 255, "%d/%m/%Y %X ", localtime(&log_clock));
  fprintf(logfile, "%s", buf);

  // The message
  va_start(args, format);
  vfprintf(logfile, format, args);
  va_end(args);

  fflush(logfile);
  fclose(logfile);
} // :log_to_file

char *alloc_printf(const char *format, ...)
{
  int size = 0;
  char *p = NULL;
  va_list ap;

  /* Determine required size */

  va_start(ap, format);
  size = vsnprintf(p, size, format, ap);
  va_end(ap);

  if (size < 0)
    return NULL;

  size++; /* For '\0' */
  p = malloc(size);
  if (p == NULL)
    return NULL;

  va_start(ap, format);
  size = vsnprintf(p, size, format, ap);
  if (size < 0)
  {
    free(p);
    return NULL;
  }
  va_end(ap);

  return p;
}

int spurious_message(const char *needle, const char *format, ...)
{
  va_list args;
  char *string;
  char *result;

  va_start(args, format);
  string = alloc_printf(format, args);
  va_end(args);

  result = strstr(string, needle);

  free(string);

  return result != NULL;
}