/*
    This file is part of libsdlvnc.

    libsdlvnc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libsdlvnc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include "sdlvnc_thread.h"

struct sdlvnc_thread_data
{
    pthread_mutex_t mut;
    int mutoldtype;
    pthread_cond_t mutcond;
};

void sdlvnc_unlock_mutex(void *p)
{
    pthread_mutex_unlock(p);
}
#undef SDLVNC_LOCK
#define SDLVNC_LOCK(data)                                              \
    assert(data);                                                      \
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &data->mutoldtype); \
    pthread_cleanup_push(sdlvnc_unlock_mutex, (void *)&data->mut);     \
    pthread_mutex_lock(&data->mut);

#undef SDLVNC_UNLOCK
#define SDLVNC_UNLOCK(data) \
    pthread_cleanup_pop(1); \
    pthread_setcanceltype(data->mutoldtype, NULL);

#undef SDLVNC_SIGNAL
#define SDLVNC_SIGNAL(data) \
    pthread_cond_signal(&data->mutcond);

#undef SDLVNC_SIG_WAIT
#define SDLVNC_SIG_WAIT(data)         \
    pthread_cond_wait(&data->mutcond, \
                      &data->mut);

#undef SDLVNC_SIG_WAIT_TIMED
#define SDLVNC_SIG_WAIT_TIMED(data, time)  \
    pthread_cond_timedwait(&data->mutcond, \
                           &data->mut,     \
                           time);

sdlvnc_thread_data *sdlvnc_get_threading(rfbClient *client)
{
    if (!client)
        return NULL;

    sdlvnc_thread_data *data = rfbClientGetClientData(client,
                                                      SDLVNC_THREADING_DATA);
    if (!data)
    {
        data = malloc(sizeof(sdlvnc_thread_data));
        pthread_mutex_init(&data->mut, NULL);
        pthread_cond_init(&data->mutcond, NULL);
        sdlvnc_set_threading(client, data);
    }
    assert(data);
    return data;
}

void sdlvnc_set_threading(rfbClient *client, sdlvnc_thread_data *data)
{
    rfbClientSetClientData(client, SDLVNC_THREADING_DATA, data);
}

void sdlvnc_clear_threading(rfbClient *client)
{
    sdlvnc_thread_data *data = rfbClientGetClientData(client,
                                                      SDLVNC_THREADING_DATA);
    if (data)
        free(data);
}