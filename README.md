# README #

SDLVNC is a library for remote desktop using VNC Protocol and display using SDL Toolkit. It comprises of libsdlvnc, sdlvnc, and wxvnc. libsdlvnc is the library for connecting to VNC server. libsdlvnc provides basic SDL interface for VNC windowing. sdlvnc implements libsdlvnc as an application. wxvnc is another implementation of libsdlvnc using wxWidgets instead of SDL.

### How do I get set up? ###

* Contact for help

### Contribution guidelines ###

* Portable, practical, simplistic, thread-safe, embeddable.

### Who do I talk to? ###

* Development: Rona Dini Hari (rona.dinihari@gmail.com)
* Support: Support at Huedawn (shackmaster@huedawn.me)